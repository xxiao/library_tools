CC = /usr/bin/icpc
CFLAGS  = -std=c++11
INCLUDES = -I/home/xiaoxian/libs/tbb44/source/tbb44_20151115oss/include
LFLAGS = -L/home/xiaoxian/libs/tbb44/source/tbb44_20151115oss/build/linux_intel64_gcc_cc4.8_libc2.19_kernel3.19.0_release
LIBS = -ltbb -ltbbmalloc
TARGET = tbb_mem
SRCS = tbb_mem.cpp
OBJS = $(SRCS:.c=.o)
$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) $(OBJS) $(LFLAGS) $(LIBS) -o $(TARGET)
clean:
	$(RM) *.o *~ $(TARGET)
