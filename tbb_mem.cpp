/*
 * Tesing TBB on issues in allocating memory
*/

#include <iostream>
#include <vector>
#include <array>
#include <sys/time.h>
#include <math.h>
#include <fstream>
#include <stdlib.h>

#include <tbb/tbb.h>
#include <tbb/scalable_allocator.h>

using namespace std;
using namespace tbb;

#define USE_TBB 1
#define ALLOC_MEM 1

#define N 100
typedef std::array< std::array< double, N >, N > Matrix;


void start_timer( timeval& i_startTimer )
{
    gettimeofday( &i_startTimer, NULL );
}

double stop_timer( timeval& i_startTimer )
{
    timeval end;
    gettimeofday( &end, NULL );
    return ( ( (          end.tv_sec * 1000000 +          end.tv_usec ) -
               ( i_startTimer.tv_sec * 1000000 + i_startTimer.tv_usec ) ) / 1000000.0);
}

double log_process_size()
{
    std::ifstream in("/proc/self/statm");
    size_t pages(0);
    in >> pages;
    if (!in) {
        cout<<"Couldn't read /proc/self/statm"<<endl;
        return -1.;
    }
    return double(pages*4096/double(1<<30));
}

void kernel( const Matrix &m1, const Matrix &m2, Matrix &result )
{
#ifdef ALLOC_MEM
    // allocate local memory with std::vector. Changing to tbb allocator
    // didn't improve the performance

    typedef vector< double, cache_aligned_allocator< double > > Row;
    vector< Row, cache_aligned_allocator< Row > > local_result(N, Row(N, 0 ) );

#endif

    for( int i = 0; i < N; i++ ){
        for( int j = 0; j < N; j++ ){
            for( int k = 0; k < N; k++ ){
#ifdef ALLOC_MEM
                local_result[i][j] += m1[i][k]* m2[k][j];
#else
                result[i][j] = m1[i][k]* m2[k][j];
#endif
            }
        }
    }
}

void multiply_matrix()
{
    const long count = 3e+4;
    Matrix  matrix1, matrix2;
    for( int i = 0; i < N; i++ ){
        for( int j = 0; j < N; j++ ){
            matrix1[ i ][ j ] = double( rand() % N );
            matrix2[ i ][ j ] = double( rand() % N );
        }
    }
    vector< Matrix > results( count );

#if USE_TBB
    tbb::parallel_for( tbb::blocked_range<int>( 0, count ), [&]( tbb::blocked_range<int> &r ){
        for( int i = r.begin(); i != r.end(); ++i ){
            kernel( matrix1, matrix2, results[ i ]);
        }
    });
#else
    for( int i = 0; i < count; i++ ){
      kernel( matrix1, matrix2, results[ i ] );
    }
#endif
}

int main()
{
    double start_mem = log_process_size();
    timeval t;
    start_timer( t );
    multiply_matrix();

#ifdef USE_TBB
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, NULL);
#endif

    cout<<" running multiply_matrix takes "<<stop_timer( t )<<" seconds "<<endl;
    cout<<" left over memory "<<log_process_size() - start_mem<<" Gb "<<endl;

    return 0;
}

